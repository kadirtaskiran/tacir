# Tacir Kullanımı

### Yeni tacir açmak için:
```shell
screen -dmS "A" -L -Logfile logs/A.log python3 run.py A
```

### Çalışan tacirleri listelemek için:
```shell
screen -ls
```

### Çalışan tacire bağlanmak için:
```shell
screen -xS Tacir_adı
```

### Çalışan tacire bağlandıktan sonra kırmak için:
 Tacire bağlandıktan sonra Ctrl+C tuşlarına basılır

### Çalışan tacire bağlandıktan sonra KIRMADAN çıkmak için:
 Tacire bağlandıktan sonra Ctrl+A+D tuşlarına basılır

### Çalışan tacirin loglarına bakmak için:
```shell
#Canlı Takip için
tail -f logs/Tacir_adi.log

#Tüm logları görmek için
cat logs/Tacir_adi.log
```